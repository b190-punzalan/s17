console.log("hello world");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function welcomeMessage(){
	let fullName = prompt("What is your full name?");
	let userAge = prompt("How old are you?");
	let location = prompt("Where do you live?");
	console.log("Hello, " + fullName);
	console.log("You are " + userAge + " " + "years old.");
	console.log("You live in " + location);
};
welcomeMessage();
alert("Thank you for your inputs!");


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printArtistList(){
		console.log("1. BTS");
		console.log("2. BlackPink");
		console.log("3. IU");
		console.log("4. Spice Girls");
		console.log("5. Backstreet Boys");
	};
	printArtistList();
// /*
// 	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
// 		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
// 		-invoke the function to display your information in the console.
// 		-follow the naming conventions for functions.
	
// */
	
// 	//third function here:
	function printMovie(){
		console.log("1. The Shawshank Redemption");
		console.log("Rotten Tomato Rating: 91%");
		console.log("2.Harry Potter and Deathly Hallows");
		console.log("Rotten Tomato Rating: 96%");
		console.log("3. Harry Potter and the Sorcerer's Stone");
		console.log("Rotten Tomato Rating: 81%");
		console.log("4. A Star is Born");
		console.log("Rotten Tomato Rating: 90%");
		console.log("Sound of Music");
		console.log("Rotten Tomato Rating: 83%");
	};
	printMovie();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){;
// let printFriends(); = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();

// console.log(friend1);
// console.log(friend2);
