// function - lines/blocks of codes that tell our device/application to perform certain tasks when called/invoked
// functions are mostly created tp create complicated tasks to run several lines of code in succession. they are used to prevent repeating lines/blocks of codes that perform the same task/function
	// declaration - defiintion of a function, create lang. 
	// invocation - need iinvoke (pagtawag) si function para ma-execute. to invoke a declared function, add the name of the function

console.log("hello world");
// function declaration
/*function keyword - defines a function in JS; indicator that we are creating a function that will be invoked later in our codes.
printName() - functionName; functions are named to be able to use later in the code; gagamitin pa uli mamaya, pag walang pangalan, hindi natin sila matatawag
function block{} - the statements inside the curly brace which comprise of the body of the function. this is where the codes are to be executed
default SYNTAX:
function functionName(){
	function statements/code block;
};
*/
function printName(){
	console.log("My name is John");
}; 
// very important ang mga semicolon, don't forget

// let's call/invole the function
/*the code block and statements inside the function is not immediately executed when the function is defined/declared. the code block/statements inside a function is executed when the function is invoked/called. kaya kukunin/tatawagin mo uli un function name, sa sample below printName()
It is common to use the term "call a function" instead of "invoke a function"
*/
printName();

// results in an error due to non-existence of the function(not yet declared)

// DECLARATION VS. EXPRESSION
// Function declaration - using function keyword and functionName
declaredFunction(); 
	// functions can be hoisted
// functions in JS can be used before it can be defined, thus hoisting is allowed for the JS functions
// NOTE: hoisting is javascript's behavior for certain variable and functions to run/use them before their declaration
function declaredFunction(){
	console.log("Hello from declaredFunction");
};

// Function Expression
// a function can also be created by storing it inside a variable. But when we try to call it before its creation, magkakaron ng access error.
// does not allow hoisting
// a function exppression is an anonymous function that is assigned to the variableFunction 
	// anonymous function - unnamed function
// variableFunction(); - calling a function that is created through an expression before it can be defined will result in an error since it is technically a variable
let variableFunction = function(){
	console.log("hello again!");
};

variableFunction();

// mini activity
function topAnime(){
	console.log("Fushigii, Ghost Fighter, Sailormoon");
};
topAnime();

function topMovies(){
	console.log("Harry Potter, Movie2, Movie 3");
};
topMovies();

// reassigning declared functions
// we can also reassign declared functions and function expressions to new anonymous functions

declaredFunction = function(){
	console.log("updated declaredFunction");
};
declaredFunction();

// However, we cannot change the declared functions/function expressions that are defined using const
const constFunction=function(){
	console.log("Initialized const function");
};
constFunction();

// If we re-assign const like below, mag-error lang siya:
// constFunction=function(){
// 	console.log("cannot be re-assigned");
// };
// constFunction();

// Function scoping
/*scope is the accessiblty og variables/functions*/
/*JS variables/functions have 3 scopes
1. local/block scope - can be accessed inside the curly brace/code block{}
2.global scope - outermost part of the codes, so anything that is written in the .js file. it does not belong to any code block  and is outside of curly braces kaya maaaccess siya; pero it can also be accessed inside any curly brace/code block
3. */

{let 
	localVar="Armando Perez";
}
let globalVar="Mr. Worldwide";

console.log(globalVar);
// console.log(localVar);

// function scope
	// JS has also function scope: each function creates a new scope. Bariables defined inside a function can only be acessed and ised inside that function. variables declared with var, let, and const are quite similar when declared inside a function- di sila pwedeng gamitin outside of their function
function showNames(){
	// function-scoped variables
	var functionVar="Joe";
	const functionConst="John";
	let functionLet="Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);
// functionVar, functionConst, and functionLet are function-scoped and cannot be accessed outside the function they are declared. kaya nag-error kanina nung icall sila

// Nested Function
// functions that are defined nside another function. These nested functions have functions-scope where they can only be accessed inside of the function where they are declared/defined
function newFunction(){
	let name="Jane";

	function nestedFunction(){
		let nestedName="John";
		console.log(name);
		console.log(nestedName);
	};
	nestedFunction();
};
newFunction();
// nestedFunction();
	// di ma-call si nested function sa sample sa taas kasi nasa labas siya ng function kung san siya dineclare kaya siya tinawag na nested function

// mini-activity
// global-scoped variable
/*create a function
	ceate another let variable
	log in the console
call the function*/ 

let globalVar2 = "joch";

function newTeam(){
	let newTeam="Rye";
	console.log(globalVar2);
	console.log(newTeam);
};
newTeam();


// using alert ()
	// alert allows us to show a small window at the top of our browser page to show info to our users as opposed to a console.log() which only shows the message on the console. parang dialog box. fixed na siya sa top of our page. directly nakikita ng users even without dev tools. It allows us to show a short dialog or instructions to our user. The page will wait (continue to load) until the user dismisses the dialog
alert("Hello World");
	// will show as the page loads, unless pindutin na ung ok button

// we can use an alert() to show a message to the user from a later function invocation
function showSampleAlert(){
	alert("Hello Again!");
};

showSampleAlert();
// we will find that the page waits for the user to dismiss the dialog box before proceeding. we can witness this by reloading the page while the console is open

console.log("I will be displayed after the has been closed.");

// NOTES on USING alert()
/*show only an alert( for short dialogs/messages to the user
do not overuse alert() because the program has to wait for it to be dismissed before continuing*/
	
// using prompt()
// prompt is used to allow us to a show a small window at the top of the browser page to gather user input. Much like alert(), it will have the page wait until the user completes or enters the input. the input from the promot will be returned as a string data type once the user dismisses the window. so lilitaw siya sa console as string
/*
Syntax:
	prompt("<dialoginString>")
*/

let samplePrompt=prompt("Enter your name.");

console.log("Hello" + samplePrompt);
// to check kung string nga, wpede mong
console.log(typeof samplePrompt);
// ang irereturn dapat niya sa console ay string

let nullPrompt=prompt("do not enter anything here");
console.log(nullPrompt);

// returns an empty string if the user clicks OK button without entering any info and null for user who click the cancel button in the window

// Notes on using prompt()
	/*it can be used to gather user input and be used in our code. However, since it will have the page wait until the user finished or closed the window. It must not be overused.
	prompt() used globally will run immediately. So for better usage and user experience, it is better to store them inside a function and call whnever they are needed
	*/

function welcomeMessage(){
	let firstName = prompt("Enter first name here");
	let lastName = prompt("Enter last name here");
	console.log("Hello, " + firstName + " " + lastName);
};
welcomeMessage();

// hindi sila mag-interfere sa mga naunang nadeclare na varaibles na same name like firstName kasi under sila ng function

//Function Naming
/*
Function names should be: 
1. definitive of the task that it will perform. It usually contains or starts with a verb/adjective
2. camelCasing*/
function getCourses(){
	let courses=["Science 101", "Math 101", "English 101"];
	console.log(courses); 
};
getCourses();

// avoid generic names like this one below
function get(){
	let name="Jamie";
	console.log(name);
};
get();

// avoid pointless an inappropriate name for our functions
function foo(){
	console.log(25%5)
};
foo();

